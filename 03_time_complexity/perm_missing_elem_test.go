package main

import (
	"testing"
)

func TestPermMissingElem(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "MissingMiddle",
			a:    []int{2, 3, 1, 5},
			want: 4,
		},
		{
			name: "MissingBeginning",
			a:    []int{2, 3, 4, 5},
			want: 1,
		},
		{
			name: "MissingEnd",
			a:    []int{2, 3, 1, 4},
			want: 5,
		},
		{
			name: "Empty",
			a:    []int{},
			want: 1,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := permMissingElem(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
