package main

import (
	"testing"
)

var frogJmpTests = []struct {
	name string
	x    int
	y    int
	d    int
	want int
}{
	{
		name: "01",
		x:    10,
		y:    85,
		d:    30,
		want: 3,
	},
	{
		name: "02",
		x:    10,
		y:    10,
		d:    30,
		want: 0,
	},
	{
		name: "03",
		x:    10,
		y:    40,
		d:    30,
		want: 1,
	},
}

func TestFrogJmpLinear(t *testing.T) {
	for _, test := range frogJmpTests {
		t.Run(test.name, func(t *testing.T) {
			got := frogJmpLinear(test.x, test.y, test.d)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}

func TestFrogJmpConstant(t *testing.T) {
	for _, test := range frogJmpTests {
		t.Run(test.name, func(t *testing.T) {
			got := frogJmpConstant(test.x, test.y, test.d)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
