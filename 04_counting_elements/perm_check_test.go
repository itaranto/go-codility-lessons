package main

import (
	"testing"
)

func TestPermCheck(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "01",
			a:    []int{4, 1, 3, 2},
			want: 1,
		},
		{
			name: "02",
			a:    []int{4, 1, 3},
			want: 0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := permCheck(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
