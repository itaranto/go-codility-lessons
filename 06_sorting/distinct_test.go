package main

import (
	"testing"
)

func TestDistinct(t *testing.T) {
	tests := []struct {
		name string
		a    []int
		want int
	}{
		{
			name: "Example",
			a:    []int{2, 1, 1, 2, 3, 1},
			want: 3,
		},
		{
			name: "Empty",
			a:    []int{},
			want: 0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			got := distinct(test.a)
			if got != test.want {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
