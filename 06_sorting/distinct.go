package main

import (
	"slices"
)

// Write a function
//
//   func distinct(A []int) int
//
// that, given an array A consisting of N integers, returns the number of distinct values in array
// A.
//
// For example, given array A consisting of six elements such that:
//  A[0] = 2
//  A[1] = 1
//  A[2] = 1
//  A[3] = 2
//  A[4] = 3
//  A[5] = 1
//
// the function should return 3, because there are 3 distinct values appearing in array A, namely 1,
// 2 and 3.
//
// Write an efficient algorithm for the following assumptions:
//
// - N is an integer within the range [0..100,000];
// - each element of array A is an integer within the range [−1,000,000..1,000,000].

func distinct(a []int) int {
	n := len(a)

	if n == 0 {
		return 0
	}

	slices.Sort(a)

	result := 1
	for i := 1; i < n; i++ {
		if a[i] != a[i-1] {
			result += 1
		}
	}

	return result
}
