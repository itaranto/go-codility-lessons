package main

import (
	"slices"
	"testing"
)

var genomicRangeQueryTests = []struct {
	name string
	s    string
	p    []int
	q    []int
	want []int
}{
	{
		name: "Example",
		s:    "CAGCCTA",
		p:    []int{2, 5, 0},
		q:    []int{4, 5, 6},
		want: []int{2, 4, 1},
	},
}

func TestGenomicRangeQuery(t *testing.T) {
	for _, test := range genomicRangeQueryTests {
		t.Run(test.name, func(t *testing.T) {
			got := genomicRangeQueryNaive(test.s, test.p, test.q)
			if !slices.Equal(got, test.want) {
				t.Errorf("Got %v, want %v", got, test.want)
			}
		})
	}
}
